<?php

namespace sistema\GuestBookBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use sistema\GuestBookBundle\Entity\Entrada;
use sistema\GuestBookBundle\Form\FormAgregarEntradaType;

class EntradaController extends Controller
{
    
//    Observacion: habilitar extension=php_intl.dll en php.ini

    
    public function altaEntradaAction () {
//        phpinfo();
        $peticion = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $entidad = new Entrada();
        $entradas = $em->getRepository('GuestBookBundle:Entrada')->findAllActive();
        if (empty($entradas)){
            //emitir mensaje con entradas inexistentes..
        }
        $formulario = $this->createForm(new FormAgregarEntradaType(), $entidad);
        $formulario->handleRequest($peticion);
        if ($peticion->isMethod('POST')) {
//            echo "Entro aqui..";
//            die();
            if ($formulario->isValid()) {
                //Setamos Los atributos corrsponidentes:
                $entidad->setEstadoActivo(1);
                $entidad->setFechaPublicacion(new \DateTime("today"));
                $em->persist($entidad);
                $em->flush();
                
                 //Envio Correo a la cuenta del los novios                     
//                $salida = $this->renderView(
//                    'GuestBookBundle:BackEnd:plantillaCorreo.html.twig',
//                        array('entrada' => $entidad));
//                $message = \Swift_Message::newInstance()
//                ->setSubject('Aviso por nueva entrada')
//                ->setFrom('valentin.sanrafael@gmail.com')
//                ->setTo('valentin.comodororivadavia@acricana.com')
//                ->setBcc('valentin.comodororivadavia@gmail.com')
//                ->setBody($salida, 'text/html')
//                ;
//                $this->get('mailer')->send($message);
                //FIN Envio correo a los novios
                
                return $this->redirect($this->generateUrl('guest_book_visualizar_entradas'));
            }
        }
        
        return $this->render('GuestBookBundle:FrontEnd:formVisualizarAgregarEntradas.html.twig', array(
            'entradas' => $entradas,
            'formulario' => $formulario->createView()
        ));
    }
    
    public function visualizarEntradaAction($id_entrada){
        $em = $this->getDoctrine()->getManager();
        $entrada = $em->getRepository('GuestBookBundle:Entrada')->find($id_entrada);
        if (empty($entrada)){
            //reridigir a un 404.
        }        
        return $this->render('GuestBookBundle:FrontEnd:visualizarEntrada.html.twig', array(
            'entrada' => $entrada
        ));
    }
    
    
}
