<?php

namespace sistema\GuestBookBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FormAgregarEntradaType extends AbstractType {
     public function buildForm(FormBuilderInterface $builder, array $options) {         
            $builder
                    ->add('nombre', 'text', array('required' => false))    
                    ->add('correo', 'email', array('required' => false))    
                    ->add('mensaje', 'textarea', array('required' => false))
                    ->add('guardar', 'submit', array('label' => 'Guardar mensaje..')) 
            ;        
            
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array('data_class' => 'sistema\GuestBookBundle\Entity\Entrada',
            'csrf_protection' => false,
            'csrf_field_name' => '_token',
            'intention' => 'task_item',));
    }
    public function getName() {
        return 'GuestBookBundle_Entrada';
    }
}

?>
